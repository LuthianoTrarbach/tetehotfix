package  ${package}.adapter.outbound;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ${package}.config.broker.BrokerOutput;
import ${package}.domain.Sample;
import ${package}.port.outbound.PortOutbound;

@Service
@Transactional
public class SampleAdapterOutboundBroker implements PortOutbound {
	
	@Autowired
	BrokerOutput output;

	@Override
	public void sendMessage(String message) {
		
		Sample sample = new Sample();
		sample.setId(message);
		
		output.outPutBroker().send(MessageBuilder.withPayload(sample).build());
	}
}
